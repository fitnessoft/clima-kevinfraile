//llamados propios de ionic y adicional jquery
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import * as $ from "jquery";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  latitud: any;
  longitud: any;
  pass = "79f3c4db4e76b526b6c728a069f9e416";
  constructor(public alertCtrl: AlertController, public loadingCtrl: LoadingController, private geolocation: Geolocation, public navCtrl: NavController) {

  }
  //se inicia la carga del pograma realizando el llamado a la funcion q activa las otras
  ionViewDidLoad() {
    this.popupcarga();
  }
  //Se obtiene la ubicación del dispositivo
  Posicionamiento() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
    }).catch((error) => {
      console.log('error al obtener la ubicacion', error);
      //notificación del error al obtener la ubicación
      this.errorubicacion();
    });

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      //se consume el servicio del estado del clima enviando las variable de latitud y longitud.
      this.ConsulotarClima(data.coords.latitude, data.coords.longitude);

    });
  }

  //Se obtiene el estado del clima por medio de ajax, y se usa jquery para poder mostrar en la vista
  ConsulotarClima(latitud, longitud) {
    let myData: any;
    $.ajax({
      url: 'https://api.darksky.net/forecast/' + this.pass + '/' + latitud + ',' + longitud,
      data: myData,
      type: 'GET',
      crossDomain: true,
      dataType: 'jsonp',
      success: function (data) {
        $('#lugar').text(data['timezone']);
        $('#sumarry').text(data['currently']['summary']);
        $('#rain').text(data['currently']['precipProbability']);
        $('#humidity').text(data['currently']['humidity']);
        $('#temperature').text(data['currently']['temperature']);
      },
      error: function () { alert('Error en el servicio'); }
    });


  }

  //llamado de la funcion click refrescar() para recargar la pagina
  refrescar() {
    this.popupcarga();
  }

  //Popup de carga, que inicia la cadena de funcionamiento del programa, ejecutando los demas servicios
  popupcarga() {
    const load = this.loadingCtrl.create({
      content: "Cargando...",
      duration: 3000
    });
    this.Posicionamiento();
    load.present();
  }

  errorubicacion(){
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Error al obtener la ubicación',
      buttons: ['Dismiss']
    });
    alert.present();
  }



}
